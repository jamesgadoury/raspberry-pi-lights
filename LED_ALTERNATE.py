import RPi.GPIO as GPIO
import time
import signal
import sys

def signal_handler(sig, frame):
    off_left()
    off_right()
    sys.exit(0)

def setup():
    signal.signal(signal.SIGINT, signal_handler)
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(17, GPIO.OUT)
    GPIO.setup(18, GPIO.OUT)

def on_left():
    GPIO.output(18, GPIO.HIGH)

def on_right():
    GPIO.output(17, GPIO.HIGH)

def off_left():
    GPIO.output(18, GPIO.LOW)

def off_right():
    GPIO.output(17, GPIO.LOW)

def light_loop():
    while True:
        on_left()
        off_right()
        time.sleep(0.5)
        on_right()
        off_left()
        time.sleep(0.5)

if __name__=="__main__":
    setup()
    light_loop()
    
