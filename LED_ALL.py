import RPi.GPIO as GPIO
import time
import signal
import sys

def signal_handler(sig, frame):
    off()
    sys.exit(0)

def setup():
    signal.signal(signal.SIGINT, signal_handler)
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(17, GPIO.OUT)
    GPIO.setup(18, GPIO.OUT)

def on():
    GPIO.output(17, GPIO.HIGH)
    GPIO.output(18, GPIO.HIGH)

def off():
    GPIO.output(17, GPIO.LOW)
    GPIO.output(18, GPIO.LOW)

def light_loop():
    while True:
        on()
        time.sleep(0.5)
        off()
        time.sleep(0.5)

if __name__=="__main__":
    setup()
    light_loop()
    
